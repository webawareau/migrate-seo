<?php
/*
Plugin Name: Migrate SEO
Description: migrate SEO settings
Version: 0.0.1
Author: WebAware
Author URI: https://shop.webaware.com.au/
*/

/*
copyright (c) 2020 WebAware Pty Ltd (email : support@webaware.com.au)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

namespace webawareau\plugin\seo;

if (!defined('ABSPATH')) {
	exit;
}

add_action('admin_init', function() {
	require __DIR__ . '/classes/seo-base.php';
	require __DIR__ . '/classes/seo-aioseo.php';
	require __DIR__ . '/classes/seo-theseoframework.php';
});

add_action('admin_menu', function() {
	add_management_page('Migrate SEO', 'Migrate SEO', 'manage_options', 'migrate-seo', __NAMESPACE__ . '\\show_migrate');
});

function show_migrate() {
	$results = false;

	if (!empty($_POST['action'])) {
		check_admin_referer('migrate', 'migrate_seo_wpnonce');

		if ($_POST['action'] === 'migrate') {
			migrate_settings();
			$results = perform_migration();
		}
	}

	require __DIR__ . '/views/migrate.php';
}

function perform_migration() {
	$types = get_post_types([
		'public'             => true,
		'publicly_queryable' => true
	], 'names', 'or');

	$posts = get_posts([
		'post_type'			=> $types,
		'post_status'		=> 'publish',
		'posts_per_page'	=> -1,
	]);

	$aioseo = new AllInOneSEO();
	$theseo = new TheSEOFramework();
	$results = [
		'examined'	=> 0,
		'migrated'	=> 0,
	];

	foreach ($posts as $post) {
		$aioseo->load_post($post->ID);

		$results['examined']++;
		if ($aioseo->has_seo()) {
			$theseo->copy($aioseo);
			$theseo->save_post($post->ID);
			$results['migrated']++;
		}
	}

	return $results;
}

function migrate_settings() {
	$aioseo = get_option('aioseop_options');
	$theseo = get_option('autodescription-site-settings');

	$theseo['homepage_title']			= $aioseo['aiosp_home_title'] ?? '';
	$theseo['homepage_description']		= $aioseo['aiosp_home_description'] ?? '';
	$theseo['google_verification']		= $aioseo['aiosp_google_verify'] ?? '';
	$theseo['bing_verification']		= $aioseo['aiosp_bing_verify'] ?? '';
	$theseo['yandex_verification']		= $aioseo['aiosp_yandex_verify'] ?? '';
	$theseo['baidu_verification']		= $aioseo['aiosp_baidu_verify'] ?? '';
	$theseo['pint_verification']		= $aioseo['aiosp_pinterest_verify'] ?? '';
	$theseo['knowledge_logo_url']		= $aioseo['aiosp_schema_organization_logo'] ?? '';

	$opengraph = $aioseo['modules']['aiosp_opengraph_options'] ?? false;
	if ($opengraph) {
		$theseo['knowledge_type']			= ($opengraph['aiosp_opengraph_person_or_org'] ?? '') === 'org' ? 'organization' : 'person';
		$theseo['homepage_og_description']	= $opengraph['aiosp_opengraph_description'] ?? '';
		$theseo['social_image_fb_url']		= $opengraph['aiosp_opengraph_dimg'] ?? '';
		$theseo['twitter_site']				= $opengraph['aiosp_opengraph_twitter_site'] ?? '';
		$theseo['twitter_card']				= $opengraph['aiosp_opengraph_defcard'] ?? 'summary';
	}

	update_option('autodescription-site-settings', $theseo);
}
