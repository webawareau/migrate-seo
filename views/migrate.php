<?php
if (!defined('ABSPATH')) {
	exit;
}
?>

<div class="wrap">
	<h1>Migrate SEO</h1>

	<?php if ($results): ?>
	<div class="notice notice-success">
		<ul>
			<li>Examined: <?= number_format_i18n($results['examined']) ?></li>
			<li>Migrated: <?= number_format_i18n($results['migrated']) ?></li>
		</ul>
	</div>
	<?php endif; ?>

	<form action="<?= admin_url('admin.php?page=migrate-seo'); ?>" method="POST">
		<p>
			<input type="checkbox" name="action" id="migrate_seo_action" value="migrate" />
			<label for="migrate_seo_action">copy SEO settings from All In One SEO to The SEO Framework</label>
		</p>

		<?php submit_button(); ?>
		<?php wp_nonce_field('migrate', 'migrate_seo_wpnonce', false); ?>
	</form>
</div>
