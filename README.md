# Migrate SEO

Created quickly in an afternoon to help migrate some WordPress websites from All In One SEO to The SEO Framework. Please feel free to fork. No support is offered.

## Migrating from All In One SEO to The SEO Framework

* deactivate All In One SEO
* install and activate The SEO Framework
* install and activate this plugin from the .zip file
* visit the SEO settings page and save, to ensure the default settings exist
* visit Tools > Migrate SEO and tick the checkbox, then submit
* visit the SEO settings page and make any required manual adjustments
* deactivate and delete this plugin
* delete All In One SEO
