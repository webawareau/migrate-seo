<?php
namespace webawareau\plugin\seo;

if (!defined('ABSPATH')) {
	exit;
}

const THESEO_TITLE				= '_genesis_title';
const THESEO_DESC				= '_genesis_description';
const THESEO_NOINDEX			= '_genesis_noindex';
const THESEO_NOFOLLOW			= '_genesis_nofollow';
const THESEO_NOARCHIVE			= '_genesis_noarchive';
const THESEO_IMAGE_URL			= '_social_image_url';

class TheSEOFramework extends SEO_Base {

	public function load_post($post_id) {
		$this->clear();

		$this->title		= $this->string_or_null(get_post_meta($post_id, THESEO_TITLE, true));
		$this->description	= $this->string_or_null(get_post_meta($post_id, THESEO_DESC, true));
		$this->noindex		= $this->to_boolean(get_post_meta($post_id, THESEO_NOINDEX, true));
		$this->nofollow		= $this->to_boolean(get_post_meta($post_id, THESEO_NOFOLLOW, true));
		$this->noarchive	= $this->to_boolean(get_post_meta($post_id, THESEO_NOARCHIVE, true));
		$this->image_url	= $this->string_or_null(get_post_meta($post_id, THESEO_IMAGE_URL, true));
	}

	public function save_post($post_id) {
		$this->save_meta($post_id, THESEO_TITLE, $this->title);
		$this->save_meta($post_id, THESEO_DESC, $this->description);
		$this->save_meta($post_id, THESEO_NOINDEX, $this->from_boolean($this->noindex));
		$this->save_meta($post_id, THESEO_NOFOLLOW, $this->from_boolean($this->nofollow));
		$this->save_meta($post_id, THESEO_NOARCHIVE, $this->from_boolean($this->noarchive));
		$this->save_meta($post_id, THESEO_IMAGE_URL, $this->image_url);
	}

	private function to_boolean($bool) {
		if ($bool === '1') {
			return true;
		}
		if ($bool === '-1') {
			return false;
		}
		return null;
	}

	private function from_boolean($bool) {
		if ($bool === true) {
			return '1';
		}
		if ($bool === false) {
			return '-1';
		}
		return null;
	}

}
