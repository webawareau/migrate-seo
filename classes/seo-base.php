<?php
namespace webawareau\plugin\seo;

if (!defined('ABSPATH')) {
	exit;
}

abstract class SEO_Base {

	public $title;
	public $description;
	public $keywords;
	public $noindex;			// true / false / null
	public $nofollow;			// true / false / null
	public $noarchive;			// true / false / null
	public $image_url;

	abstract public function load_post($post_id);

	abstract public function save_post($post_id);

	protected function get_vars() {
		return array_keys(get_object_vars($this));
	}

	public function clear() {
		foreach ($this->get_vars() as $name) {
			$this->$name = null;
		}
	}

	public function has_seo() {
		foreach ($this->get_vars() as $name) {
			if ($this->$name) {
				return true;
			}
		}
		return false;
	}

	public function copy(SEO_Base $src) {
		foreach ($this->get_vars() as $name) {
			$this->$name = $src->$name;
		}
	}

	protected function save_meta($post_id, $key, $value) {
		if ($value === null) {
			delete_post_meta($post_id, $key);
		}
		else {
			update_post_meta($post_id, $key, $value);
		}
	}

	protected function string_or_null($string) {
		$string = trim($string);
		if ($string === '') {
			return null;
		}
		return $string;
	}

}
