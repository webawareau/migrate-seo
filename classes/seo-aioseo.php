<?php
namespace webawareau\plugin\seo;

if (!defined('ABSPATH')) {
	exit;
}

const AIOSEO_TITLE				= '_aioseop_title';
const AIOSEO_DESC				= '_aioseop_description';
const AIOSEO_KEYWORDS			= '_aioseop_keywords';
const AIOSEO_NOINDEX			= '_aioseop_noindex';
const AIOSEO_NOFOLLOW			= '_aioseop_nofollow';
const AIOSEO_META				= '_aioseop_opengraph_settings';
const AIOSEO_META_IMAGE_URL		= 'aioseop_opengraph_settings_image';

class AllInOneSEO extends SEO_Base {

	public function load_post($post_id) {
		$this->clear();

		$this->title		= $this->string_or_null(get_post_meta($post_id, AIOSEO_TITLE, true));
		$this->description	= $this->string_or_null(get_post_meta($post_id, AIOSEO_DESC, true));
		$this->keywords		= $this->string_or_null(get_post_meta($post_id, AIOSEO_KEYWORDS, true));
		$this->noindex		= $this->to_boolean(get_post_meta($post_id, AIOSEO_NOINDEX, true));
		$this->nofollow		= $this->to_boolean(get_post_meta($post_id, AIOSEO_NOFOLLOW, true));

		$this->post_type = get_post_field('post_type', $post_id);

		$meta = get_post_meta($post_id, AIOSEO_META, true);
		if (is_array($meta)) {
			$this->image_url	= $meta[AIOSEO_META_IMAGE_URL] ?? null;
		}
	}

	public function save_post($post_id) {
	}

	private function to_boolean($bool) {
		if ($bool === 'on') {
			return true;
		}
		return null;
	}

}
